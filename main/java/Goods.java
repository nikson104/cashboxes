import java.util.ArrayList;

public class Goods {
    static final String s1[] = {"Meat", "Milk", "Bread", "Beer", "Fish", "Cheeps", "Coffee", "Chocolate", "Rice",
            "Cigarettes"};
    private static ArrayList<String> goods;

    public static ArrayList<String> goods() {
        goods = new ArrayList();
        int k = (int) (Math.random() * 10);
        for (int i = 0; i < k; i++) {
            int j = (int) (Math.random() * 10);
            goods.add(s1[j]);
        }
        return goods;
    }
}