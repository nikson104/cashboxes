
public class Main {
    public static void main(String[] args) {
        CashBox cashBox1 = new CashBox("CashBox 1");
        CashBox cashBox2 = new CashBox("CashBox 2");
        CashBoxPool cashBoxPool = new CashBoxPool(cashBox1, cashBox2);
        for (int i = 1; i < 6; i++) {
            Customer customer = new Customer("Customer " + i, cashBoxPool, Goods.goods());
            Thread customer1 = new Thread(customer);
            customer1.start();
        }
    }
}