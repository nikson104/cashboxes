import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CashBoxPool {
    private List<CashBox> сashBoxes;

    public CashBoxPool(CashBox... cashBoxes) {
        сashBoxes = new ArrayList<>(Arrays.asList(cashBoxes));
    }

    public synchronized CashBox getCashBox() {
        while (сashBoxes.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        CashBox сashBox = сashBoxes.remove(0);
        return сashBox;
    }

    public synchronized void put(CashBox cashBox) {
        сashBoxes.add(cashBox);
        System.out.println(cashBox.getName() + " is free");
        notifyAll();
    }
}