import java.util.ArrayList;
import java.util.List;

public class Customer implements Runnable {
    private CashBoxPool cashBoxPool;
    private String name;
    private List<String> goods;

    public Customer(String name, CashBoxPool cashBoxPool, ArrayList<String> goods) {
        this.name = name;
        this.cashBoxPool = cashBoxPool;
        this.goods = goods;
    }

    public String getName() {
        return this.name;
    }

    public void run() {
        System.out.println(name + " came in ");
        CashBox cashBox = null;
        try {
            cashBox = cashBoxPool.getCashBox();
            cashBox.serviceClient(this);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            cashBoxPool.put(cashBox);

        }
    }

    public List<String> getGoods() {
        return goods;
    }
}