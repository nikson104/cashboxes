public class CashBox {
    private String name;

    public CashBox(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void serviceClient(Customer client) throws InterruptedException {
        System.out.println(client.getName() + " took " + name);
        System.out.println(name + "; Customer: " + client.getName() + "; Purchases: " + client.getGoods());
        Thread.sleep(2000);
    }
}